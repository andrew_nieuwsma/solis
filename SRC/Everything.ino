/***********************
*@author Andrew Nieuwsma, Developed for MSSE 2018
*Some code is via adafruit, some code is from class, all other code (the majority) is by the author
************************/

/*
 * @TODO - fix button 'bounce'/sensitivity
 *       - take advanatage of program mem (or is it flash?) to store debug messages as a way to conserve memory
 */

/***********************
*INCLUDE HEADER FILES
************************/
#include <Adafruit_GFX.h>	// Core graphics library for TFT
#include <SPI.h>		//Used for talking to the touch screen - via SPI
#include <Adafruit_ILI9341.h>
#include <Adafruit_STMPE610.h>
#include <Wire.h>
#include "RTClib.h"		// Date and time functions using a DS1307 RTC connected via I2C and Wire lib
#include <Adafruit_NeoPixel.h>	//neopixel
#ifdef __AVR__
#include <avr/power.h>
#endif

/***********************
*DEFINE DEBUG VERBOSITY
************************/
//#define DEBUG //Uncomment to include, but READ warning first!
/* !!!WARNING THE HIGHER THE VERBOSITY LEVEL THE GREATER STRESS ON THE SYSTEM AND THE INCREASED LIKELIHOOD OF MISSING TASKS!!!
 *  !!! WARNING - DEBUG STATEMENTS TAKE MEMORY, so BE CAREFUL!!!
 * V0 = NO OUTPUT
 * V1 = INITIALIZATION MESSAGES ONLY 
 * V2 = TOP LEVEL TASK FUNCTION CALLS FROM INSIDE FUNCTION
 * V3 = NON-TOP LEVEL TASKS
 * V4 = BRANCHING LOGIC
 * V5 = DETAILED DEBUG MESSAGES
 */
enum debug_verbosity { V0 = 0, V1 = 1, V2 = 2, V3 = 3, V4 = 4, V5 = 5 };
debug_verbosity debug_level = V4;

/***********************
*DEFINE NEOPIXEL
************************/
#define NEOPIXEL_PIN 6		//This will be used for the pin to set the neopixels
#define NEOPIXEL_COUNT 60	//60 leds

/***********************
*DEFINE TOUCHSCREEN
*This is calibration data for the raw touch data to the screen coordinates
************************/
#define TOUCHSCREEN_MINX 150
#define TOUCHSCREEN_MINY 130
#define TOUCHSCREEN_MAXX 3800
#define TOUCHSCREEN_MAXY 4000
#define STMPE_CS 8		// The STMPE610 uses hardware SPI on the shield, and #8
#define TFT_CS 10		// The display also uses hardware SPI, plus #9 & #10
#define TFT_DC 9

char daysOfTheWeek[7][12] = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };

/***********************
*INITIALIZE - TFT, TS, RTC, NEOPIXELS
************************/
//REAL TIME CLOCK
RTC_DS1307 rtc;

//TFT
Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC);

//TOUCH SCREEN
Adafruit_STMPE610 ts = Adafruit_STMPE610(STMPE_CS);

//DEFINE TOUCHSCREEN DEBOUNCE VALUES
long debounceDelay = 30;
int MINPRESSURE = 20;
int MAXPRESSURE = 1000;

//NEOPIXELS
// Parameter 1 = number of pixels in strip
// Parameter 2 = Arduino pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NEOPIXEL_COUNT, NEOPIXEL_PIN, NEO_GRB + NEO_KHZ800);

/***********************
*DEFINE UI BOUNDARIES AND ELEMENTS
************************/

/***********************
* COLOR BOXES
************************/
#define BOXSIZE 40
#define BOX_Y BOXSIZE*2
#define BOX_X BOXSIZE*6
#define BOX_Y_OFFSET 0
#define BOX_X_OFFSET 0

int currentcolor =0;
int desiredColor;

/***********************
* DEFINE COLORS FOR COLORBOX TO LED MAPPING
************************/
typedef struct {
	uint8_t r;
	uint8_t g;
	uint8_t b;
} ColorValue;

typedef struct {
	uint16_t x1;
	uint16_t x2;
	uint16_t y1;
	uint16_t y2;
} Location;

typedef struct {
	uint16_t TouchScreenColor;
	ColorValue ColorVal;
	Location Loc;
} ColorStruct;

#define RED 0
#define ORANGE 1
#define YELLOW 2
#define GREENYELLOW 3
#define GREEN 4
#define CYAN 5
#define BLUE 6
#define MAGENTA 7
#define PURPLE 8
#define MAROON 9
#define PINK 10
#define WHITE 11
#define COLOR_SIZE 12

ColorStruct colors[COLOR_SIZE];

/***********************
* SWITCH LED ON / OFF
************************/
#define SWITCHSIZE_Y  80
#define SWITCHSIZE_X  120
#define SWITCH_Y_OFFSET  80
unsigned long LAST_SWITCH_TIME = 0;
unsigned long LAST_SWITCH_MIN_TIME = 500;	//software debounce, sort of
bool CURRENT_PIXEL_STATE = false;	// true is on, false is off
bool flagIntentToSwitchState = false;
//bool LAST_PIXEL_STATE = true;
enum intended_led_state { OFF_STATE = 0, ON_STATE = 1 };
intended_led_state ils = OFF_STATE;

/***********************
* LED TIMER
************************/
#define TIMER_Y 80
#define TIMER_X 120
#define TIMER_Y_OFFSET 160
#define TIMER_X_OFFSET 0
//bool TIMER_ENABLED = false;
DateTime led_off_at;
enum led_timer { FiveSecond = 5, ThirtySecond = 30, FifteenMin = 900, ThirtyMin = 1800, SixtyMin = 3600, NinetyMin =
	    5400, NoTimer = -1
};
led_timer l_timer = NoTimer;
bool hasLEDTimerExpired = false;

/***********************
* LED BRIGHTNESS
************************/
#define BRIGHTNESS_Y  80
#define BRIGHTNESS_X  120
#define BRIGHTNESS_Y_OFFSET  80
#define BRIGHTNESS_X_OFFSET 120
enum brightness_enum { BRIGHTNESS_20Percent = 51, BRIGHTNESS_40Percent = 102, BRIGHTNESS_60Percent =
	    153, BRIGHTNESS_80Percent = 204, BRIGHTNESS_100Percent = 255 };
brightness_enum brightness_value = BRIGHTNESS_100Percent;

/***********************
* BACKLIGHT TIMER
************************/
#define BACKLIGHT_Y  80
#define BACKLIGHT_X  120
#define BACKLIGHT_Y_OFFSET  160
#define BACKLIGHT_X_OFFSET 120
DateTime backlight_off_at;
enum backlight_timer { Ten = 10, Thirty = 30, One = 60, Two = 120, Five = 300, None = -1 };
backlight_timer bl_timer = Ten;
bool backlightON = true;
bool hasBacklightTimerExpired = false;
#define TS_BACKLIGHT 3

/***********************
* TASTE THE RAINBOW
************************/
#define RAINBOW_Y 80
#define RAINBOW_X 240
#define RAINBOW_Y_OFFSET 240
#define RAINBOW_X_OFFSET 0

/***********************
* DEFINE TASK ELEMENTS
************************/
int task_count = 0;
unsigned long ttime_ms = 0;
#define MAX_PRIORITY 250
#define MAX_TASKS 10
enum job_state { BLOCKING = 0, READY = 1, RUNNING = 2 };
enum tasks_status { RUN = 0, REPORT = 1 };

//TASK STRUCT
typedef struct {
	int (*funptr) ();
	int period;		// milliseconds
	uint32_t next_release;	// absolute time of next release in ms
	int missed_deadlines;
	int id;
	int priority;		// priority 1 has the highest priority
	int buffered;		// the number of jobs waiting to execute
	int max_buffered;	// maxim bufferend while system running
	int releases;		// number of times released
	int executed;		// number of times executed
	int state;		// one of the 3 states
} Task;

volatile Task tasks[MAX_TASKS];
int task_id;
int temp;
int highest;

/***********************
* SETUP - runs once on boot, the goes to LOOP
************************/
void setup(void)
{
	//Start Serial
	Serial.begin(9600);
	writeDebugMessage(V1, "Arduino Powered Neopixel Lamp - Andrew Nieuwsma | MSSE | Spring 2018 \r\n");
	initColors();

	//Start TFT and Touch Screen 
	tft.begin();
	if (!ts.begin()) {
		while (1) ;
	}
	writeDebugMessage(V1, "Touch screen started\r\n");

	setupBacklightPin();
	paintScreenBlack();

	paintColors();

	//Start the clock
	Wire.begin();
	if (!rtc.begin()) {
		while (1) ;
	}
	if (!rtc.isrunning()) {
		rtc.adjust(DateTime(2018, 4, 10, 17, 0, 0));	// THE TIME WILL BE RESET TO 2/10/2018
	}

	writeDebugMessage(V1, "RTC is RUNNING\r\n");

	//initialize items on screen, painting them one by one 
	//paintClock(); decided not needed
	paintLEDStatusOFF();
	paintBrightnessButton();
	paintLEDTimer();
	resetBacklightTimer();
	paintBacklightTimer();
	paintTheRainbow();

	//start the leds, turn them off
	writeDebugMessage(V1, "Initializing LEDs to OFF!\r\n");

	strip.begin();
	strip.show();		// Initialize all pixels to 'off'
	colorWipe(strip.Color(0, 0, 0), 0);

	//spawn all tasks and prepare to enter the loop
	spawn_all_tasks();

	writeDebugMessage(V1, "System Initialized!\r\n");
}

/***********************
* loop in arduino is the main 'while' loop that runs forever
* in this case it is specifically purposed to do a single thing - manage tasks
* this means determining timings and running the tasks on schedule.
************************/
void loop()
{

	//get the time
	ttime_ms = millis();

	// Check the task table to see if anything is ready to release
	//i dont have access to the timer0 interrupt, but I think if all tasks run < 1 ms its not a problem, and if it is greater than I just need to check if its elapsed or not.
	for (int i = 0; i < task_count; i++) {
		if (tasks[i].next_release <= ttime_ms) {
			tasks[i].releases += 1;
			if (tasks[i].state != BLOCKING) {
				tasks[i].missed_deadlines++;
			}
			tasks[i].state = READY;
			tasks[i].next_release = tasks[i].period + ttime_ms;
			tasks[i].buffered += 1;
			if (tasks[i].buffered > tasks[i].max_buffered) {
				tasks[i].max_buffered = tasks[i].buffered;
			}
		}
	}
	// Determine highest priority ready task
	task_id = -1;
	highest = MAX_PRIORITY + 1;
	for (int i = 0; i < task_count; i++) {
		cli();
		temp = tasks[i].state;
		sei();
		if (temp == READY) {
			if (tasks[i].priority < highest) {
				task_id = i;
				highest = tasks[i].priority;
			}
		}
	}
	// Execute the task, then do housekeeping in the task array
	if (-1 != task_id) {
		cli();
		tasks[task_id].state = RUNNING;
		sei();
		tasks[task_id].funptr();

		cli();
		tasks[task_id].executed += 1;
		tasks[task_id].buffered -= 1;
		if (tasks[task_id].buffered > 0) {
			tasks[task_id].state = READY;
		} else {
			tasks[task_id].state = BLOCKING;
		}
		sei();
	}

}

/***********************
* writeDebugMessage
* This is the main way to use Serial.print, i have it setup with a debug verbosity, 
* so that if I dont care about messages I dont have to print them
* ONLY prints if system verbosity is >= to message verbosity
* param[in] debug_verbosity - msgVerbosity(0-5)
* param[in] String - message
************************/
int writeDebugMessage(debug_verbosity msgVerbosity, String msg)
{
	if (msgVerbosity <= debug_level) {
		Serial.print(msg);
	}
}

/***********************
* pickColor
* picks the color and draws the selection box
* param[in] TS_Point p- the point that was touched
************************/
int pickColor(TS_Point p)
{
	if (p.y > BOX_Y_OFFSET && p.y <= BOX_Y_OFFSET + BOX_Y && p.x <= BOX_X + BOX_X_OFFSET && p.x >= BOX_X_OFFSET) {
		paintColors();
	
		for (int i = 0; i < COLOR_SIZE; ++i) {
			if (p.x >= colors[i].Loc.x1 && p.x <= colors[i].Loc.x2 && p.y >= colors[i].Loc.y1
			    && p.y <= colors[i].Loc.y2) {
				currentcolor = colors[i].TouchScreenColor;
				desiredColor = i;
				tft.fillRect(colors[i].Loc.x1 + 18, colors[i].Loc.y1 + 18, 4, 4, ILI9341_WHITE);
			}
		}
		flagIntentToSwitchState = true;
	}
	return;
}

/***********************
* userInterface
* this is the main UI method, it registers touches on the touch screen
* , and then decided what action to take
************************/
int userInterface()
{

	if (ts.bufferEmpty()) {
		return;
	}
	// Retrieve a point
	TS_Point p = ts.getPoint();

	//ATTEMPTING TO DEBOUNCE
	delay(debounceDelay);
	p = ts.getPoint();
	//now throw away the rest of the buffer
	while (ts.touched()) {
		p = ts.getPoint();
	}

	if (p.z < MINPRESSURE || p.z > MAXPRESSURE) {
		return;
	}
	//END OF DEBOUNCE

#ifdef DEBUG
	writeDebugMessage(V4, "userInterface() - confirmed a touch\r\n");
#endif

	//there was a button press so reset the backlight timer expiration
	//but dont do anything else
	bool backlightTimerReset = false;
	if (isBacklightTimerExpired() == true) {
		backlightTimerReset = true;
	}
	resetBacklightTimer();
	if (isBacklightTimerExpired() != backlightTimerReset) {
		return;
	}
	// Scale from ~0->4000 to tft.width using the calibration #'s
	p.x = map(p.x, TOUCHSCREEN_MINX, TOUCHSCREEN_MAXX, 0, tft.width());
	p.y = map(p.y, TOUCHSCREEN_MINY, TOUCHSCREEN_MAXY, 0, tft.height());

//FIGURE OUT WHAT THE USER TOUCHED
//Change Colors

	if (p.y > BOX_Y_OFFSET && p.y <= BOX_Y_OFFSET + BOX_Y && p.x <= BOX_X + BOX_X_OFFSET && p.x >= BOX_X_OFFSET) {

#ifdef DEBUG
		writeDebugMessage(V4, "TOUCH - ColorPicker\r\n");
#endif
		pickColor(p);

	}
	//TURN ON OR OFF LEDS
	else if (p.y >= SWITCH_Y_OFFSET && p.y <= SWITCH_Y_OFFSET + SWITCHSIZE_Y && p.x <= SWITCHSIZE_X) {

#ifdef DEBUG
		writeDebugMessage(V4, "TOUCH - Switch\r\n");
#endif

		if (isLEDON() == true) {
			setLEDResource(false);
			paintLEDStatusOFF();
		} else if (isLEDON() == false) {
			setLEDResource(true);
			paintLEDStatusON();
		}

	}
	//ADJUST BRIGHTNESS
	else if (p.y > BRIGHTNESS_Y_OFFSET && p.y <= BRIGHTNESS_Y_OFFSET + BRIGHTNESS_Y
		 && p.x <= BRIGHTNESS_X + BRIGHTNESS_X_OFFSET && p.x >= BRIGHTNESS_X_OFFSET) {

#ifdef DEBUG
		writeDebugMessage(V4, "TOUCH - Brightness\r\n");
#endif
		if (isLEDON() == true) {
			cycleBrightnessSettings();
			strip.setBrightness(brightness_value);
			paintBrightnessButton();
			flagIntentToSwitchState = true;
		}
	}
	//TIMER
	else if (p.y > TIMER_Y_OFFSET && p.y <= TIMER_Y_OFFSET + TIMER_Y && p.x <= TIMER_X + TIMER_X_OFFSET
		 && p.x >= TIMER_X_OFFSET) {

#ifdef DEBUG
		writeDebugMessage(V4, "TOUCH - LEDTimer\r\n");
#endif

		cycleLedTimerSettings();
		resetLEDTimer();
		paintLEDTimer();
	} else if (p.y > BACKLIGHT_Y_OFFSET && p.y <= BACKLIGHT_Y_OFFSET + BACKLIGHT_Y
		   && p.x <= BACKLIGHT_X_OFFSET + BACKLIGHT_X && p.x >= BACKLIGHT_X_OFFSET) {

#ifdef DEBUG
		writeDebugMessage(V4, "TOUCH - BacklightTimer\r\n");
#endif

		cycleBacklightTimerSettings();
		paintBacklightTimer();
	} else if (p.y > RAINBOW_Y_OFFSET && p.y <= RAINBOW_Y_OFFSET + RAINBOW_Y && p.x <= RAINBOW_X_OFFSET + RAINBOW_X
		   && p.x >= RAINBOW_X_OFFSET) {

#ifdef DEBUG
		writeDebugMessage(V4, "TOUCH - TasteTheRainbow\r\n");
#endif

		if (isLEDON() == true) {
			tasteTheRainbow();
			flagIntentToSwitchState = true;
		}
	}
	return 0;
}

/***********************
* spawn_all_tasks
* spawn all tasks
************************/
void spawn_all_tasks()
{
	// spawn(fptr, id, period, priority)
	if (debug_level == V5) {
		spawn(&printTimeToConsole, 1, 1000, 3);
		spawn(&printTasksToConsole, 3, 5000, 4);
	}

	spawn(&manageLEDS, 2, 100, 2);
	spawn(&userInterface, 4, 75, 1);

	//spawn(&paintClock, 5, 10000, 5);
	spawn(&manageBacklightTimer, 6, 200, 6);
	spawn(&manageLEDTimer, 7, 300, 7);
	spawn(&paintLEDTimer, 8, 5000, 8);
}

/***********************
* spawn
* spawn a task
* param [ in ] - int* fp | function pointer
* param [ in ] - int id | the task id
* param [ in ] - int p | the period of the task
* param [ in ] - int priority | the priority of the task
************************/
int spawn(int (*fp) (), int id, int p, int priority)
{
	if (task_count == MAX_TASKS) {
		return -1;
	}
	tasks[task_count].funptr = fp;
	tasks[task_count].period = p;
	tasks[task_count].next_release = p;
	tasks[task_count].id = id;
	tasks[task_count].priority = priority;
	tasks[task_count].buffered = 1;
	tasks[task_count].max_buffered = 1;
	tasks[task_count].releases = 0;
	tasks[task_count].executed = 0;
	tasks[task_count].state = READY;
	++task_count;
	return 1;
}

/***********************
* setLEDResource 
* param[in] - bool stat - true = turn led on, false = turn led off
* sets the flag of intent to switch state
* set the indended value of off or on
************************/
int setLEDResource(bool stat)
{

#ifdef DEBUG
	writeDebugMessage(V2, "setLEDResource()\r\n");
	writeDebugMessage(V4, "desire led state = ");
	writeDebugMessage(V4, (String) stat);
	writeDebugMessage(V4, "\r\n");
#endif

	flagIntentToSwitchState = true;

#ifdef DEBUG
	writeDebugMessage(V4, "flag = ");
	writeDebugMessage(V4, (String) flagIntentToSwitchState);
	writeDebugMessage(V4, "\r\n");
#endif

	if (stat == true) {
		ils = ON_STATE;
	} else if (stat == false) {
		ils = OFF_STATE;
	}
}

/***********************
* paintScreenBlack
* paints the whole tft black
************************/
int paintScreenBlack()
{
  //paint the touch screen black
  tft.fillScreen(ILI9341_BLACK);
}

/***********************
* paintColors
* paints the 12 color boxes to the screen
************************/
int paintColors()
{
  for (int i = 0; i < COLOR_SIZE; ++i) {
    ColorStruct cs = colors[i];
    tft.fillRect(colors[i].Loc.x1, colors[i].Loc.y1, BOXSIZE, BOXSIZE, colors[i].TouchScreenColor);
  }
}

/***********************
* initColors
* fills the colors array with color values
************************/
int initColors()
{
	colors[RED].ColorVal = ColorValue {
	255, 0, 0};
	colors[RED].TouchScreenColor = ILI9341_RED;
	colors[RED].Loc = Location {
	0, 39, 0, 39};
	colors[ORANGE].ColorVal = ColorValue {
	255, 165, 0};
	colors[ORANGE].TouchScreenColor = ILI9341_ORANGE;
	colors[ORANGE].Loc = Location {
	40, 79, 0, 39};
	colors[YELLOW].ColorVal = ColorValue {
	255, 255, 0};
	colors[YELLOW].TouchScreenColor = ILI9341_YELLOW;
	colors[YELLOW].Loc = Location {
	80, 119, 0, 39};
	colors[GREENYELLOW].ColorVal = ColorValue {
	173, 255, 47};
	colors[GREENYELLOW].TouchScreenColor = ILI9341_GREENYELLOW;
	colors[GREENYELLOW].Loc = Location {
	120, 159, 0, 39};
	colors[GREEN].ColorVal = ColorValue {
	0, 255, 0};
	colors[GREEN].TouchScreenColor = ILI9341_GREEN;
	colors[GREEN].Loc = Location {
	160, 199, 0, 39};
	colors[CYAN].ColorVal = ColorValue {
	0, 255, 255};
	colors[CYAN].TouchScreenColor = ILI9341_CYAN;
	colors[CYAN].Loc = Location {
	200, 239, 0, 39};
	colors[BLUE].ColorVal = ColorValue {
	0, 0, 255};
	colors[BLUE].TouchScreenColor = ILI9341_BLUE;
	colors[BLUE].Loc = Location {
	0, 39, 40, 79};
	colors[MAGENTA].ColorVal = ColorValue {
	255, 0, 255};
	colors[MAGENTA].TouchScreenColor = ILI9341_MAGENTA;
	colors[MAGENTA].Loc = Location {
	40, 79, 40, 79};
	colors[PURPLE].ColorVal = ColorValue {
	128, 0, 128};
	colors[PURPLE].TouchScreenColor = ILI9341_PURPLE;
	colors[PURPLE].Loc = Location {
	80, 119, 40, 79};
	colors[MAROON].ColorVal = ColorValue {
	128, 0, 0};
	colors[MAROON].TouchScreenColor = ILI9341_MAROON;
	colors[MAROON].Loc = Location {
	120, 159, 40, 79};
	colors[PINK].ColorVal = ColorValue {
	255, 105, 180};
	colors[PINK].TouchScreenColor = ILI9341_PINK;
	colors[PINK].Loc = Location {
	160, 199, 40, 79};
	colors[WHITE].ColorVal = ColorValue {
	255, 255, 255};
	colors[WHITE].TouchScreenColor = ILI9341_WHITE;
	colors[WHITE].Loc = Location {
	200, 239, 40, 79};

	return 0;
}

/***********************
* manageLEDS
* sets the desired color on the leds
* THIS IS THE ONLY METHOD THAT SHOULD EVERY WRITE  - CURRENT_PIXEL_STATE - AS IT IS THE CRITICAL RESOURCE!!!
************************/
int manageLEDS()
{

	ColorValue d = colors[desiredColor].ColorVal;

	if (flagIntentToSwitchState == true) {

#ifdef DEBUG
		writeDebugMessage(V4, "manageLEDS()\r\n");
		writeDebugMessage(V4, "flag = ");
		writeDebugMessage(V4, (String) flagIntentToSwitchState);
		writeDebugMessage(V4, "\r\n");
		writeDebugMessage(V4, "intent = ");
		writeDebugMessage(V4, (String) ils);
		writeDebugMessage(V4, "\r\n");
#endif

		if (ils == OFF_STATE) {
			colorWipe(strip.Color(0, 0, 0), 0);
			CURRENT_PIXEL_STATE = false;
			paintLEDStatusOFF();
			l_timer = NoTimer;
			paintLEDTimer();
		} else if (ils == ON_STATE) {
			colorWipe(strip.Color(d.r, d.g, d.b), 0);
			CURRENT_PIXEL_STATE = true;
			paintLEDStatusON();
			paintLEDTimer();
		}

		flagIntentToSwitchState = false;
	}
	return 0;
}

/***********************
* paintTheRainbow
* paints the taste the rainbow button
************************/
int paintTheRainbow()
{
	tft.fillRect(RAINBOW_X_OFFSET, RAINBOW_Y_OFFSET, RAINBOW_X, RAINBOW_Y, ILI9341_MAGENTA);
	tft.setCursor(RAINBOW_X_OFFSET + 10, RAINBOW_Y_OFFSET + 10);
	tft.setTextSize(4);
	tft.setTextColor(ILI9341_WHITE);
	tft.println("Taste the Rainbow");
}

/***********************
* paintLEDTimer
* paints the timer box, and will change what it looks like based on the state of system
* this is NOT the system time, that is paintCLock
************************/
int paintLEDTimer()
{

#ifdef DEBUG
	writeDebugMessage(V4, "paintLEDTimer()\r\n");
#endif

	if (l_timer == NoTimer) {
		tft.fillRect(TIMER_X_OFFSET, TIMER_Y_OFFSET, TIMER_X, TIMER_Y, ILI9341_DARKGREY);
		tft.setCursor(TIMER_X_OFFSET + 10, TIMER_Y_OFFSET + 25);
		tft.setTextSize(2);
		tft.setTextColor(ILI9341_BLACK);
		tft.println("disabled");
		tft.setTextSize(1);
	} else {
		tft.fillRect(TIMER_X_OFFSET, TIMER_Y_OFFSET, TIMER_X, TIMER_Y, ILI9341_GREEN);
		tft.setTextSize(3);
		tft.setCursor(TIMER_X_OFFSET + 25, TIMER_Y_OFFSET + 25);
		if (l_timer != NoTimer) {

    DateTime nowish = rtc.now();
    long nowSeconds = nowish.secondstime();
    long futureSeconds = led_off_at.secondstime();
    long deltaSeconds = futureSeconds - nowSeconds;
      if(l_timer < 60)
      {
        
			tft.println(l_timer);	
		  tft.setTextSize(2);
		  tft.println(" seconds");
      } else if(l_timer >= 60)
      {
      tft.print(deltaSeconds/60);
      tft.print("/");
      tft.println(l_timer/60); 
      tft.setTextSize(2);
      tft.println(" minutes");
      }
      
   }
	}
	tft.setTextSize(2);
	tft.setTextColor(ILI9341_BLACK);
	tft.setCursor(TIMER_X_OFFSET + 5, TIMER_Y_OFFSET + 5);
	tft.print("LED Timer");

	return 0;
}

/***********************
* cycleBacklightTimerSettings
* cycles through the backlight timer enum
************************/
int cycleBacklightTimerSettings()
{

	switch (bl_timer) {
	case Ten:
		bl_timer = Thirty;
		break;
	case Thirty:
		bl_timer = One;
		break;
	case One:
		bl_timer = Two;
		break;
	case Two:
		bl_timer = Five;
		break;
	case Five:
		bl_timer = None;
		break;
	case None:
		bl_timer = Ten;
		break;
	}

}

/***********************
* cycleLedTimerSettings
* cycles through the LED timer enum
************************/
int cycleLedTimerSettings()
{
//enum led_timer { FiveSecond = 5, ThirtySecond = 30, FifteenMin = 900, ThirtyMin = 1800, SixtyMin = 3600, NinetyMin = 5400, NoTimer = -1};
	if (isLEDON() == false) {
		l_timer = NoTimer;
		return;
	}
	switch (l_timer) {
	case FiveSecond:
		l_timer = ThirtySecond;
		break;
	case ThirtySecond:
		l_timer = FifteenMin;
		break;
	case FifteenMin:
		l_timer = ThirtyMin;
		break;
	case ThirtyMin:
		l_timer = SixtyMin;
		break;
	case SixtyMin:
		l_timer = NinetyMin;
		break;
	case NinetyMin:
		l_timer = NoTimer;
		break;
	case NoTimer:
		l_timer = FiveSecond;
		break;
	}
}

/***********************
* paintBacklightTimer
* paints the backlight timer button with values
************************/
int paintBacklightTimer()
{
	tft.setTextSize(2);
	tft.fillRect(BACKLIGHT_X_OFFSET, BACKLIGHT_Y_OFFSET, BACKLIGHT_X, BACKLIGHT_Y, ILI9341_YELLOW);
	tft.setTextColor(ILI9341_BLACK);
	tft.setCursor(BACKLIGHT_X_OFFSET + 5, BACKLIGHT_Y_OFFSET + 5);
	tft.print("Backlight");
	tft.setTextSize(4);
	tft.setCursor(BACKLIGHT_X_OFFSET + 20, BACKLIGHT_Y_OFFSET + 35);
	switch (bl_timer) {
	case 10:
		tft.println("10s");
		break;
	case 30:
		tft.println("30s");
		break;
	case 60:
		tft.println("1m");
		break;
	case 120:
		tft.println("2m");
		break;
	case 300:
		tft.println("5m");
		break;
	default:
		tft.println("OFF");
		break;
	}

	return 0;
}

/***********************
*paintClock
*prints the month - day, hour : minute to the screen
*mm-dd hh:mm  (24hr)
************************/
int paintClock()
{
	tft.fillRect(0, BOXSIZE, BOXSIZE * 6, BOXSIZE, ILI9341_WHITE);
	tft.setCursor(BOXSIZE / 2, BOXSIZE + 10);
	tft.setTextColor(ILI9341_BLACK);
	tft.setTextSize(3);

	DateTime now = rtc.now();

	tft.print(now.month(), DEC);
	tft.print('-');
	tft.print(now.day(), DEC);
	tft.print("  ");
	tft.print(now.hour(), DEC);
	tft.print(':');
	tft.print(now.minute(), DEC);
	tft.println();
	return 0;
}

/***********************
* cycleBrightnessSettings
* cycles through the brightness enum
************************/
int cycleBrightnessSettings()
{
//enum brightness_enum{ BRIGHTNESS_20Percent = 51, BRIGHTNESS_40Percent = 102, BRIGHTNESS_60Percent = 153, BRIGHTNESS_80Percent = 204, BRIGHTNESS_100Percent = 255};

	switch (brightness_value) {
	case BRIGHTNESS_20Percent:
		brightness_value = BRIGHTNESS_40Percent;
		break;
	case BRIGHTNESS_40Percent:
		brightness_value = BRIGHTNESS_60Percent;
		break;
	case BRIGHTNESS_60Percent:
		brightness_value = BRIGHTNESS_80Percent;
		break;
	case BRIGHTNESS_80Percent:
		brightness_value = BRIGHTNESS_100Percent;
		break;
	case BRIGHTNESS_100Percent:
		brightness_value = BRIGHTNESS_20Percent;
		break;
	}
}

/***********************
* paintBrigtnessButton
* paints the brightness button, VERY static
************************/
int paintBrightnessButton()
{
	tft.setTextSize(2);
	tft.fillRect(BRIGHTNESS_X_OFFSET, BRIGHTNESS_Y_OFFSET, BRIGHTNESS_X, BRIGHTNESS_Y, ILI9341_BLUE);
	tft.setTextColor(ILI9341_WHITE);
	tft.setCursor(BRIGHTNESS_X_OFFSET, BRIGHTNESS_Y_OFFSET + 5);
	tft.print("Brightness");
	tft.setTextSize(4);
	tft.setCursor(BRIGHTNESS_X_OFFSET + 10, BRIGHTNESS_Y_OFFSET + 35);
	tft.print((brightness_value / 51) * 20);
	tft.print("%");
	return 0;
}

/***********************
* paintLEDStatusON
* updates the led on / off button with ON value
************************/
int paintLEDStatusON()
{

#ifdef DEBUG
	writeDebugMessage(V2, "PaintLEDStatusON()\r\n");
#endif

	tft.setTextSize(2);
	tft.fillRect(0, SWITCH_Y_OFFSET, SWITCHSIZE_X, SWITCHSIZE_Y, ILI9341_GREEN);
	tft.setCursor(15, SWITCH_Y_OFFSET + 2);
	tft.setTextColor(ILI9341_BLACK);
	tft.print("LEDs");
	tft.setTextSize(6);
	tft.setCursor(25, SWITCH_Y_OFFSET + 30);
	tft.setTextColor(ILI9341_BLACK);
	tft.print("ON");
	return 0;
}

/***********************
* paintLEDStatusOFF
* updates the led on / off button with OFF value
************************/
int paintLEDStatusOFF()
{

#ifdef DEBUG
	writeDebugMessage(V2, "PaintLEDStatusOFF()\r\n");
#endif

	tft.setTextSize(2);
	tft.fillRect(0, SWITCH_Y_OFFSET, SWITCHSIZE_X, SWITCHSIZE_Y, ILI9341_RED);
	tft.setCursor(15, SWITCH_Y_OFFSET + 2);
	tft.setTextColor(ILI9341_BLACK);
	tft.print("LEDs");
	tft.setTextSize(6);
	tft.setCursor(10, SWITCH_Y_OFFSET + 30);
	tft.setTextColor(ILI9341_BLACK);
	tft.print("OFF");
	return 0;
}

/***********************
* setBacklight
* turn the backlight off or on
* param [in] - bool stat | true = on, false = off
************************/
void setBacklight(bool stat)
{
	//pretty sure true == 1 == high
	digitalWrite(TS_BACKLIGHT, stat);
	backlightON = stat;
}

/***********************
* isBacklightTimerExpired
* calls the update timer expired function and returns state
* return - bool, if the timer is expired returns false
************************/
bool isBacklightTimerExpired()
{
	//if the timer is disabled then it will alway be false!
	updateBacklightTimerExpired();
	return hasBacklightTimerExpired;
}

/***********************
* updateBacklightTimerExpired
* if the timer watchdog is off, then the timer is NOT expired
* else if time > timer then it is expired
************************/
void updateBacklightTimerExpired()
{
	DateTime tempTime = rtc.now();
	if (tempTime.secondstime() >= backlight_off_at.secondstime() && bl_timer != None) {
		hasBacklightTimerExpired = true;
	} else {
		hasBacklightTimerExpired = false;
	}
}

/***********************
* setupBacklightPin
* backlight is on pin 3
************************/
void setupBacklightPin()
{
	pinMode(TS_BACKLIGHT, OUTPUT);
}

/***********************
* resetBacklightTimer
* reset the backlight timer to be NOW + watchdog length, 
* will be invoked on every touch screen press
************************/
void resetBacklightTimer()
{
	DateTime now = rtc.now();
	backlight_off_at = (now + TimeSpan(bl_timer));
}

bool isBacklightON()
{
	return backlightON;
}

/***********************
* manageBacklightTimer
* if the time is expired, then turn off the backlight, else turn it on
************************/
int manageBacklightTimer()
{
	if (isBacklightTimerExpired() == true) {
		setBacklight(false);
	} else {
		setBacklight(true);
	}
	return 0;
}

/***********************
* isLEDTimerExpired
* calls the update timer expired function and returns state
* return - bool, if the timer is expired returns false
************************/
bool isLEDTimerExpired()
{
	//if the timer is disabled then it will alway be false!
	return hasLEDTimerExpired;
}

/***********************
* updateLEDTimerExpired
* if the timer watchdog is off, then the timer is NOT expired
* else if time > timer then it is expired
************************/
void updateLEDTimerExpired()
{
	DateTime tempTime = rtc.now();
	if (tempTime.secondstime() >= led_off_at.secondstime() && l_timer != NoTimer) {
		hasLEDTimerExpired = true;
	} else {
		hasLEDTimerExpired = false;
	}
}

/***********************
* resetLEDTimer
* reset the LED timer to be NOW + watchdog length, 
* will be invoked on every touch screen press
************************/
void resetLEDTimer()
{
	DateTime now = rtc.now();
	led_off_at = (now + TimeSpan(l_timer));
}

/***********************
* isLEDON
* returns if the LED is on, based on the value of CURRENT_PIXEL_STATE
************************/
bool isLEDON()
{
	return CURRENT_PIXEL_STATE;
}

/***********************
* manageLEDTimer
* if the time is expired, then turn off the LED, else turn it on
************************/
int manageLEDTimer()
{
	updateLEDTimerExpired();	//calls the update
	if (isLEDTimerExpired() == true) {
		setLEDResource(false);

	}
	return 0;
}

/***********************
* PrintTasks
* print the tasks to the serial console
************************/
int printTasksToConsole()
{
	Serial.println("Task:\tMissedDeadlines:\tbuffered:\tmax_buffered:\texecuted:");
	for (int i = 0; i < task_count; ++i) {
		Serial.print(tasks[i].id);
		Serial.print("\t");
		Serial.print(tasks[i].missed_deadlines);
		Serial.print("\t");
		Serial.print(tasks[i].buffered);
		Serial.print("\t");
		Serial.print(tasks[i].max_buffered);
		Serial.print("\t");
		Serial.println(tasks[i].executed);
	}
	return 0;
}

/***********************
*printTimeToConsole
* prints the time to the serial console
* yyyy/mm/dd (day of week) hh:mm:ss
************************/
int printTimeToConsole()
{
	DateTime now = rtc.now();
	Serial.print(now.year(), DEC);
	Serial.print('/');
	Serial.print(now.month(), DEC);
	Serial.print('/');
	Serial.print(now.day(), DEC);
	Serial.print(" (");
	Serial.print(daysOfTheWeek[now.dayOfTheWeek()]);
	Serial.print(") ");
	Serial.print(now.hour(), DEC);
	Serial.print(':');
	Serial.print(now.minute(), DEC);
	Serial.print(':');
	Serial.print(now.second(), DEC);
	Serial.println();
	return 0;
}

int tasteTheRainbow()
{
	theaterChaseRainbow(0);

}

/***********************
* ADAFRUIT CODE - COMMENTED BY ANDREW
************************/

/***********************
* colorWipe
* Fill the dots one after the other with a color
* param [ in ] - uint32_t c | the color value
* param [ in ] - uint8_t wait | the desired ms wait to put between each pixel set
************************/
void colorWipe(uint32_t c, uint8_t wait)
{
	for (uint16_t i = 0; i < strip.numPixels(); i++) {
		strip.setPixelColor(i, c);
		strip.show();
		delay(wait);
	}
}

/***********************
* rainbow
* changes the color progression of the pixels to look like a rainbow
* param [ in ] - uint8_t wait | the desired ms wait to put between each pixel set
************************/
void rainbow(uint8_t wait)
{
	uint16_t i, j;

	for (j = 0; j < 256; j++) {
		for (i = 0; i < strip.numPixels(); i++) {
			strip.setPixelColor(i, Wheel((i + j) & 255));
		}
		strip.show();
		delay(wait);
	}
}

/***********************
* rainbowCycle
* changes the color progression of the pixels to look like a rainbow
* Slightly different than just rainbow, this makes the rainbow equally distributed throughout
* param [ in ] - uint8_t wait | the desired ms wait to put between each pixel set
************************/
void rainbowCycle(uint8_t wait)
{
	uint16_t i, j;

	for (j = 0; j < 256 * 5; j++) {	// 5 cycles of all colors on wheel
		for (i = 0; i < strip.numPixels(); i++) {
			strip.setPixelColor(i, Wheel(((i * 256 / strip.numPixels()) + j) & 255));
		}
		strip.show();
		delay(wait);
	}
}

/***********************
* theaterChase
* Theatre-style crawling lights.
* param [ in ] - uint32_t c | the color value
* param [ in ] - uint8_t wait | the desired ms wait to put between each pixel set
************************/
void theaterChase(uint32_t c, uint8_t wait)
{
	for (int j = 0; j < 10; j++) {	//do 10 cycles of chasing
		for (int q = 0; q < 3; q++) {
			for (uint16_t i = 0; i < strip.numPixels(); i = i + 3) {
				strip.setPixelColor(i + q, c);	//turn every third pixel on
			}
			strip.show();

			delay(wait);

			for (uint16_t i = 0; i < strip.numPixels(); i = i + 3) {
				strip.setPixelColor(i + q, 0);	//turn every third pixel off
			}
		}
	}
}

/***********************
* theaterChaseRainbow
* Theatre-style crawling lights with rainbow effect
* param [ in ] - uint8_t wait | the desired ms wait to put between each pixel set
************************/
void theaterChaseRainbow(uint8_t wait)
{
	for (int j = 0; j < 256; j++) {	// cycle all 256 colors in the wheel
		for (int q = 0; q < 3; q++) {
			for (uint16_t i = 0; i < strip.numPixels(); i = i + 3) {
				strip.setPixelColor(i + q, Wheel((i + j) % 255));	//turn every third pixel on
			}
			strip.show();

			delay(wait);

			for (uint16_t i = 0; i < strip.numPixels(); i = i + 3) {
				strip.setPixelColor(i + q, 0);	//turn every third pixel off
			}
		}
	}
}

/***********************
* Wheel
* Input a value 0 to 255 to get a color value.
* param [ in ] - byte WheelPos| The colours are a transition r - g - b - back to r.
************************/
uint32_t Wheel(byte WheelPos)
{
	WheelPos = 255 - WheelPos;
	if (WheelPos < 85) {
		return strip.Color(255 - WheelPos * 3, 0, WheelPos * 3);
	}
	if (WheelPos < 170) {
		WheelPos -= 85;
		return strip.Color(0, WheelPos * 3, 255 - WheelPos * 3);
	}
	WheelPos -= 170;
	return strip.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
}


