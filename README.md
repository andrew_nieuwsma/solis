# README #

## SOLIS v1.0##

### What is SOLIS? ###

Solis, the Latin word for light, is an Arduino driven lamp with touch screen interface. Solis is a stand-alone product and does not require any external device to control or manage it.  I became interested in developing a lamp after seeing a demonstration of NeoPixels, the RGB LEDs that provide the lamp its light.  Around the same time, I remember reading about the human circadian rhythm, and how light can affect our body’s natural wake/sleep cycle. The project I demonstrate today is the 2nd version of Solis. I chose to re-develop Solis because I was unsatisfied with the result of the project from several years ago.

The original prototype for Solis was also a physical lamp that was Arduino driven, but the original design used a LCD screen and 5 tactile buttons. However, the implementation of my first version had to be very scaled back because of memory, performance & timing problems I encountered in development.  The embodiment of version one abandoned the display and the features that relied on it and used a potentiometer to control brightness and a button to control color.  

In taking SENG 5831 and learning about different ways that tasks could be periodically scheduled I was curious if I could revive the concept of the lamp and make it something more useful. While I did rely on my memory of the original project, I did not use any previous code base or designs in this project, it is all ‘fresh’ and of my own composition (aside from the scheduling algorithm and some code snippets from a vendor- see References for a list).


### Who do I talk to? ###

* If you have comments or questions feel free to email: andrew (at) nieuwsma (dot) net